## React Naive Bootstrap-Like Component

#### Installation
- please use as a submodule
```
git submodule add https://gitlab.com/augusts/rn_bootstrap_component src/rnbc
```
- copy one of `*-plugins.js` from `src/rnbc/*-plugins.js` to `src/plugins.js`
- install all the packages imported in `plugins.js`

#### How to use
```javascript
// use component
import Button from "../rnbc/components/button";

// use common styles package
import styles from "../rnbc/styles";
```
