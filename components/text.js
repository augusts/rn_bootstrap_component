import React from 'react';
import { Text } from '../../plugins';
import styles from '../styles';

export default ({ color = 'dark', style, children, ...props }) => (
  <Text {...props} style={[styles.color(`${color}`), styles.p].concat(style)}>
    {children}
  </Text>
);
