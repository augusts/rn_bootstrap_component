import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import styles from '../../styles';
import {
  TouchableOpacity,
  ImagePicker,
  ToastAndroid,
  DocumentPicker,
  PDFView,
  Image,
  FetchBlob,
  Modal,
  View
} from '../../../plugins';
import Alert from '../alert';
import Button from '../button';
import Text from '../text';
import ZoomableView from '../zoomableView';
import { Row, Col } from '../layout';
import Icon from '../../icon';

export default class FileInput extends Component {
  state = {
    show: false,
    value: this.props.value
  };
  togglePreview = () => {
    const { show } = this.state;
    this.setState({ show: !show });
  };
  onDownloadPress = async uri => {
    try {
      ToastAndroid.show('Start Downloading...', ToastAndroid.SHORT);
      const extension = uri
        .replace(/^data:(.*?)\/(.*?);base64.*$/, '$2')
        .toLowerCase();
      const fileName = `${randomStr(5)}${Date.now().toString()}`;

      const res = await FetchBlob.fs.writeFile(
        `${FetchBlob.fs.dirs.DCIMDir}/${fileName}.${extension}`,
        uri.replace(/^data:.*?;base64,/, ''),
        'base64'
      );
    } catch (e) {
      Alert.danger(e.message);
    }
    function randomStr(length = 5) {
      const c =
        'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
      return Array.from(
        { length },
        _ => c[Math.floor(Math.random() * c.length)]
      ).join('');
    }
  };
  onRemovePress = () => {
    const { onChange } = this.props;
    this.setState({ value: undefined });
    onChange(undefined);
  };
  onPress = () => {
    const { title, path, onChange } = this.props;

    ImagePicker.showImagePicker(
      {
        title,
        customButtons: [{ name: 'file', title: 'Choose File' }],
        storageOptions: {
          skipBackup: true,
          path
        }
      },
      res => {
        const { didCancel, error, customButton } = res;
        if (error) {
          Alert.danger(error.message);
          return;
        }
        if (didCancel) return;
        if (customButton) {
          this.selectDocument();
        } else {
          const base64 = res.data;
          res.uri = `data:${res.type};base64,${base64}`;
          this.setState({ value: res });
          onChange(res);
        }
      }
    );
  };
  selectDocument = async () => {
    const { imageOnly } = this.props;
    try {
      const file = await DocumentPicker.pick({
        readContent: true,
        type: !!imageOnly ? 'image/*' : '*/*'
      });
      await this.onDocument(file);
    } catch (e) {
      Alert.danger(e.message);
    }
  };
  onDocument = async file => {
    const { onChange, maxSize } = this.props;
    try {
      const size = file.size || 0;
      if (size > maxSize) throw new Error('File Too Large');

      const base64 = await FetchBlob.fs.readFile(file.uri, 'base64');
      file.uri = `data:${file.type};base64,${base64}`;
      this.setState({ value: file });
      onChange(file);
    } catch (e) {
      Alert.danger(e.message);
    }
  };
  render() {
    const { disabled, style, imageOnly } = this.props;
    const { value } = this.state;
    return (
      <Fragment>
        <TouchableOpacity
          activeOpacity={0.5}
          onPress={!disabled && this.onPress}
          style={[
            styles.center,
            styles.border(1),
            styles.rounded(5),
            styles.backgroundColor('white'),
            styles.padding(5),
            {
              opacity: !disabled ? 1 : 0.5,
              aspectRatio: 2,
              minWidth: 200,
              minHeight: 100
            }
          ].concat(style)}
        >
          {!!value ? (
            <Fragment>
              {typeof value === 'string' && this.renderURLDisplayer(value)}
              {typeof value !== 'string' && this.renderFileDisplayer(value)}
            </Fragment>
          ) : (
            <Icon
              name={imageOnly ? 'images' : 'file-plus'}
              size={styles.h1.fontSize}
              color={'dark-bg'}
            />
          )}
          {this.renderUtilButtons()}
        </TouchableOpacity>
        {this.renderModal()}
      </Fragment>
    );
  }
  renderFileDisplayer(file) {
    const { uri, fileName, filename, name } = file;
    const extension = (fileName || filename || name || '')
      .split('.')
      .pop()
      .toLowerCase();
    switch (extension) {
      case 'pdf':
        return (
          <PDFView
            style={[styles.fill]}
            resource={file.uri.replace(/data:(.*?);base64,/, '')}
            resourceType={'base64'}
          />
        );
      case 'png':
      case 'jpg':
      case 'jpeg':
        return (
          <Image
            style={[styles.fill, styles.backgroundColor('light')]}
            source={file}
          />
        );
      default:
        return (
          <Row fy={'center'}>
            <Icon name={'file'} style={[styles.marginRight(15)]} />
            <Text>{filename || name}</Text>
          </Row>
        );
    }
  }
  renderURLDisplayer(uri) {
    const isBase64 = uri.match(/^data/);
    const extension = isBase64
      ? uri.replace(/^data:(.*?)\/(.*?);base64.*$/, '$2').toLowerCase()
      : uri
          .split('.')
          .pop()
          .toLowerCase();

    switch (extension) {
      case 'pdf':
        return (
          <PDFView style={[styles.fill]} resource={uri} resourceType={'url'} />
        );
      case 'png':
      case 'jpg':
      case 'jpeg':
        return (
          <Image
            style={[styles.fill, styles.backgroundColor('light')]}
            source={{ uri }}
          />
        );
      default:
        const filename = uri.split('/').pop();
        return (
          <Row fy={'center'}>
            <Icon name={'file'} style={[styles.marginRight(15)]} />
            <Text>{filename}</Text>
          </Row>
        );
    }
  }
  renderUtilButtons() {
    const { value } = this.state;
    if (!value) return null;

    const uri = typeof value === 'string' ? value : value.uri;
    const isBase64 = uri.match(/^data/);
    const extension = isBase64
      ? uri.replace(/^data:(.*?)\/(.*?);base64.*$/, '$2').toLowerCase()
      : uri
          .split('.')
          .pop()
          .toLowerCase();
    const isImage = !!~['jpg', 'png', 'jpeg'].indexOf(extension);

    return (
      <Fragment>
        <Col
          style={[styles.absolute({ right: 10, top: 5 }), { opacity: 0.75 }]}
        >
          <Button color={'danger'} onPress={this.onRemovePress}>
            <Icon name={'times'} />
          </Button>
        </Col>
        <Col
          style={[
            styles.absolute({ left: 10, top: 5 }),
            {
              opacity: 0.75
            }
          ]}
        >
          {!!isImage && (
            <Button color={'light'} onPress={this.togglePreview}>
              <Icon name={'expand'} />
            </Button>
          )}
          <Button color={'light'} onPress={() => this.onDownloadPress(uri)}>
            <Icon name={'arrow-down'} />
          </Button>
        </Col>
      </Fragment>
    );
  }
  renderModal() {
    const { show, value } = this.state;
    if (!show || !value) return null;
    return (
      <Modal
        supportedOrientations={['portrait', 'landscape']}
        visible={show}
        onRequestClose={this.togglePreview}
      >
        <ZoomableView
          controls={false}
          style={[styles.backgroundColor('black')]}
        >
          <TouchableOpacity
            onPress={this.togglePreview}
            activeOpacity={1}
            style={[styles.vh(1), styles.vw(1)]}
          >
            {typeof value === 'string' && this.renderURLDisplayer(value)}
            {typeof value !== 'string' && this.renderFileDisplayer(value)}
          </TouchableOpacity>
        </ZoomableView>
      </Modal>
    );
  }
}
FileInput.propTypes = {
  style: PropTypes.any,
  onChange: PropTypes.func,
  value: PropTypes.any,
  title: PropTypes.string,
  path: PropTypes.string,
  disabled: PropTypes.bool,
  maxSize: PropTypes.number,
  imageOnly: PropTypes.bool
};
FileInput.defaultProps = {
  title: 'Select Image',
  path: 'images',
  onChange: _ => _,
  maxSize: 20971520,
  imageOnly: false
};
