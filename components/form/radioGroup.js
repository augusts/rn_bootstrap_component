import React from 'react';
import { View, TouchableOpacity } from '../../../plugins';
import Icon from '../../icon';

import styles from '../../styles';
import Label from './label';

export const Radio = ({
  disabled,
  checked,
  value,
  color = 'primary',
  label = '',
  style,
  onPress
}) => {
  return (
    <TouchableOpacity
      activeOpacity={0.5}
      style={[
        styles.row,
        styles.marginRight(10),
        !!disabled && { opacity: 0.5 },
        { alignItems: 'center' }
      ].concat(style)}
      onPress={!disabled && onPress}
    >
      <Icon size={20} name={'circle'} color={!!checked ? color : 'dark'} />
      {!!checked && (
        <Icon
          style={[styles.marginLeft(-16)]}
          size={12}
          name={'circle-s'}
          color={color}
        />
      )}
      <Label style={[styles.marginLeft(5)]}>{label}</Label>
    </TouchableOpacity>
  );
};

export default ({
  style,
  value,
  disabled,
  row = false,
  color = 'primary',
  children,
  onPress
}) => {
  const items = [];
  React.Children.forEach(children, (child, i) => {
    items.push(
      React.cloneElement(child, {
        key: i,
        color: child.props.color || color,
        checked: value === child.props.value,
        disabled: disabled || child.props.disabled,
        onPress: () => onPress && onPress(child.props.value)
      })
    );
  });
  return (
    <View style={[!!row && { ...styles.row, flexWrap: 'wrap' }].concat(style)}>
      {items}
    </View>
  );
};
