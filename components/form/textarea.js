import React from 'react';
import { TextInput } from '../../../plugins';
import styles from '../../styles';

export default ({
  style,
  disabled,
  editable,
  multiline = true,
  numberOfLines = 1,
  spellCheck = true,
  ...props
}) => {
  const _disabled = !(!disabled || editable);
  return (
    <TextInput
      {...props}
      spellCheck={spellCheck}
      editable={!_disabled}
      multiline={multiline}
      numberOfLines={numberOfLines}
      style={[
        styles.border(1),
        styles.rounded(5),
        styles.padding(6, 12),
        styles.backgroundColor(!_disabled ? 'white' : 'bg'),
        styles.color('dark'),
        styles.p,
        { maxHeight: 150 }
      ].concat(style)}
    />
  );
};
