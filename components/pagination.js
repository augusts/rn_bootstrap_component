import React from 'react';
import Button from './button';
import ButtonGroup from './buttonGroup';
import Text from './text';
import Icon from '../icon';

export default ({
  style,
  color = 'primary',
  current = 1,
  total = 15,
  display = 5,
  onPress
}) => {
  let displayHalf = ~~(display / 2);
  const allowLeft = current - displayHalf > 1,
    allowRight = current + displayHalf < total;

  return (
    <ButtonGroup style={style} color={color}>
      <Button
        outline
        disabled={!allowLeft}
        onPress={() => onPress && onPress(1)}
      >
        <Icon name={'chevron-double-left'} />
      </Button>
      {false}
      {Array(display)
        .fill(null)
        .map((_, i) => {
          if (current + displayHalf >= total) return total - display + i + 1;
          return Math.max(current - displayHalf, 1) + i;
        })
        .filter(i => i >= 1 && i <= total)
        .map(i => (
          <Button
            key={i}
            outline={i !== current}
            onPress={() => i !== current && onPress && onPress(i)}
          >
            <Text>{i}</Text>
          </Button>
        ))}
      <Button
        outline
        disabled={!allowRight}
        onPress={() => onPress && onPress(total)}
      >
        <Icon name={'chevron-double-right'} />
      </Button>
    </ButtonGroup>
  );
};
