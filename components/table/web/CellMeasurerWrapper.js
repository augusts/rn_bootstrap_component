import React from 'react';
import { FlatList } from '../../../../plugins';

export default ({ children, ...props }) => {
  return <FlatList.CellMeasurer {...props}>{children}</FlatList.CellMeasurer>;
};
