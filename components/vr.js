import React from 'react';
import { View, StyleSheet } from '../../plugins';
import { Row, Flex } from './layout';
import styles from '../styles';

export default ({
  style,
  lineStyle,
  count = 1,
  color = 'dark',
  transparent = false
}) => (
  <Row style={[styles.margin(0, 2)].concat(style)}>
    {Array(count)
      .fill(null)
      .map((_, i) => (
        <View
          key={i}
          style={[
            styles.marginLeft(i === 0 ? 16 : 1),
            styles.marginRight(i === count - 1 ? 16 : 1),
            {
              borderRightWidth: StyleSheet.hairlineWidth,
              borderRightColor: transparent
                ? 'transparent'
                : styles.getColorCode(`darken-0.1-${color}-bg`)
            }
          ].concat(lineStyle)}
        />
      ))}
  </Row>
);
